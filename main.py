from fastapi import FastAPI
import sqlite3
from fastapi.staticfiles import StaticFiles
app = FastAPI()


app.mount("/allImage/images", StaticFiles(directory="images"), name="static")

# print(StaticFiles(directory="images"))

@app.get("/allImage")
async def getAllImagePath():
    #Connecting to sqlite
    connection = sqlite3.connect('database/database.sql')
    #Creating a cursor object using the cursor() method
    cursor = connection.cursor()
    # Preparing SQL queries to INSERT a record into the database.
    cursor.execute("SELECT * FROM informationCustommers")
    # Get all value in cursor
    values = cursor.fetchall()
    # Commit your changes in the database
    connection.commit()
    # Closing the connection
    connection.close()
    return values



