# import sqlite3

# database = sqlite3.connect("")
# query = "SELECT * FROM informationCustommers"
# a = database.execute(query)
# database.commit()
# for i in a:
#     print(i)
# database.close()


import sqlite3

#Connecting to sqlite
connection = sqlite3.connect('database/database.sql')

#Creating a cursor object using the cursor() method
cursor = connection.cursor()

# Preparing SQL queries to INSERT a record into the database.
cursor.execute("SELECT * FROM informationCustommers")
values = cursor.fetchall()
# Commit your changes in the database
connection.commit()
# Closing the connection
connection.close()
print(type(values))
